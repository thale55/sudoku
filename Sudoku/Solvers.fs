namespace Sudoku

open Types

module SolvedCell =
    // All solved cells from the groups
    let getRules (game: Game) =
        seq {
            for group in game do
                for cell in group do
                    if cell.IsSolved then yield cell
        } |> Seq.distinct |> Seq.map (fun cell -> struct (cell, Group.empty))

module HiddenValue =
    // All solved values obtained by converting groups into single-id rules
    let getRules (game: Game) =
        seq {
            for group in game do
                for rule in Group.asSingleIdRules group do
                    if rule.IsSolved then yield struct (rule, group)
        } |> Seq.distinctBy fstVal

module SingleValue =
    // All values that cover no more than three cells obtained by converting groups into single-id rules
    let getRules (game: Game) =
        seq {
            for group in game do
                for rule in Group.asSingleIdRules group do
                    if rule.Tags.Count <= 3 then yield struct (rule, group)
        } |> Seq.distinctBy fstVal

module SolvedSet =
    // All perfect subsets from the groups
    let getRules (game: Game) =
        seq {
            for group in game do
                yield!
                    group
                    |> Group.unsolvedCells
                    |> Seq.allSubsets 5
                    |> Seq.map Seq.sum
                    |> Seq.where _.IsPerfect
                    |> Seq.map (fun rule -> struct (rule, Group.empty)) }

module HiddenSet =
    // All perfect subsets obtained by converting groups into single-id rules
    let getRules (game: Game) =
        seq {
            for group in game do
                yield!
                    group
                    |> Group.asSingleIdRules
                    |> Seq.allSubsets 3
                    |> Seq.map Seq.sum
                    |> Seq.where _.IsPerfect
                    |> Seq.map (fun rule -> struct (rule, group)) }

module XWing =
    // Generalized X-Wing
    let getRules (game: Game) =
        seq {
            // All single-id rules with two tags
            let idRules =
                game
                |> Seq.collect Group.asSingleIdRules
                |> Seq.where (Rule.tagCount >> (=) 2)
                |> Seq.groupBy _.OnlyId
                |> Map.ofSeq
            for id in idRules.Keys do
                for struct (({ Tags = tagsA } as ruleA), ({ Tags = tagsB } as ruleB)) in
                        idRules[id] |> Seq.distinct |> Seq.pairings do
                    let a1, a2 = tagsA.MinimumElement, tagsA.MaximumElement
                    let b1, b2 = tagsB.MinimumElement, tagsB.MaximumElement
                    if [a1; a2; b1; b2] |> List.distinct |> List.length = 4 then
                        if Game.inCommonGroup a1 b1 && Game.inCommonGroup a2 b2 then
                            let markers = Set.ofArray [| ruleA; ruleB |]
                            yield struct (Rule.fromSingleId (Set.ofArray [| a1; b1 |]) id, markers)
                            yield struct (Rule.fromSingleId (Set.ofArray [| a2; b2 |]) id, markers)
                        if Game.inCommonGroup a1 b2 && Game.inCommonGroup a2 b1 then
                            let markers = Set.ofArray [| ruleA; ruleB |]
                            yield struct (Rule.fromSingleId (Set.ofArray [| a1; b2 |]) id, markers)
                            yield struct (Rule.fromSingleId (Set.ofArray [| a2; b1 |]) id, markers)
        }

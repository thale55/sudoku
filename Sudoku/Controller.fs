﻿namespace Sudoku

open System

type Settings = { Enabled: bool list; ShowChanges: bool }
type Control =
    | Show
    | Solve of Solver list
    | SolveAll of Solver list
    | SolveOnce of Solver
    | Drop
    | Quit
type Modifier = ConsoleModifiers

module Controller =
    let allSolvers = [
        { Name = "Solved Cell"; GetRules = SolvedCell.getRules }
        { Name = "Hidden Value"; GetRules = HiddenValue.getRules }
        { Name = "Single Value"; GetRules = SingleValue.getRules }
        { Name = "Solved Set"; GetRules = SolvedSet.getRules }
        { Name = "Hidden Set"; GetRules = HiddenSet.getRules }
        { Name = "X-Wing"; GetRules = XWing.getRules } ]

    let private (|ValidNumber|_|) = function
        | ConsoleKey.D1 | ConsoleKey.NumPad1 -> Some 1
        | ConsoleKey.D2 | ConsoleKey.NumPad2 -> Some 2
        | ConsoleKey.D3 | ConsoleKey.NumPad3 -> Some 3
        | ConsoleKey.D4 | ConsoleKey.NumPad4 -> Some 4
        | ConsoleKey.D5 | ConsoleKey.NumPad5 -> Some 5
        | ConsoleKey.D6 | ConsoleKey.NumPad6 -> Some 6
        | _ -> None

    let inline private toggle index ({ Enabled = enabled } as settings) =
        { settings with Enabled = List.updateAt index (not enabled[index]) enabled }

    let private printSettings solverNames rule { Enabled = enabled } =
        enabled
        |> Seq.iteri (fun i isEnabled ->
            let n = allSolvers[i].Name
            let f = if isEnabled then id else Io.dim
            let g = if Set.contains n solverNames then Io.hilite else id
            printf $"  {(f << g) n}")
        if Rule.notEmpty rule then
            let s = Io.rule($"Rule {rule}")
            printf $"     {s}"

    let inline private getSolvers { Enabled = enabled } =
        [ for isEnabled, solver in Seq.zip enabled allSolvers do if isEnabled then yield solver ]

    let initialSettings = { Enabled = List.replicate allSolvers.Length true; ShowChanges = true }

    [<TailCall>]
    let rec control weight solverNames rule ({ ShowChanges = showChanges } as settings) =
        Io.eraseLine()
        printf $" -> %3d{weight} game weight."
        printSettings solverNames rule settings
        let keyInfo = Console.ReadKey true
        match keyInfo.Key, keyInfo.Modifiers with
        | ValidNumber n, Modifier.None -> SolveOnce allSolvers[n - 1], settings
        | ValidNumber n, Modifier.Control -> control weight solverNames rule (toggle (n - 1) settings)
        | ConsoleKey.C, Modifier.None -> Show, { settings with ShowChanges = not showChanges}
        | (ConsoleKey.Spacebar | ConsoleKey.NumPad0), Modifier.None -> Solve (getSolvers settings), settings
        | ConsoleKey.Enter, Modifier.None -> SolveAll (getSolvers settings), settings
        | (ConsoleKey.Backspace | ConsoleKey.B), Modifier.None -> Drop, settings
        | (ConsoleKey.Q | ConsoleKey.Escape), Modifier.None -> Quit, settings
        | _, _ -> control weight solverNames rule settings

﻿module Sudoku.Io

open System
open System.IO

type Argument =
    | IsFile of string
    | IsDirectory of string * string array
    | IsNothing

let checkArgument () : Argument =
    let argv = Environment.GetCommandLineArgs()

    if argv.Length < 2 then
        IsNothing
    else
        let name = argv[1]

        if File.Exists name then
            IsFile name
        elif Directory.Exists name then
            IsDirectory (name, argv[2..])
        else
            let filepath = Path.Combine(DirectoryInfo(Path.GetDirectoryName argv[0])
                .Parent.Parent.Parent.Parent.ToString(), "data", name)
            if File.Exists filepath then
                IsFile filepath
            else
                IsNothing

let loadRules filepath : Rules =
    let s = File.ReadLines filepath |> String.concat ""
    if s.Length <> 81 then
        failwithf $"{filepath} is not a valid sudoku data file"
    let pairs = s |> Seq.mapi (fun tag ch -> (tag, if ch = ' ' then 0 else int ch - int '0'))
    seq { for tag, id in pairs do if id > 0 then yield Rule.fromSolved tag id }

// 1 1,2 1-3, 1,2,4-5 -3 3-
let parseRuns totalSolvers =
    Seq.map (fun (run:String) ->
        [for item in run.Split(',', ';') do
            try
                if item.StartsWith('-') then
                    let n = Int32.Parse item[1..]
                    yield! [1..n]
                elif item.EndsWith('-') then
                    let m = Int32.Parse(item.TrimEnd('-'))
                    yield! [m..totalSolvers]
                elif item.Contains('-') then
                    let mn = item.Split('-') |> Array.map Int32.Parse
                    yield! [mn[0]..mn[1]]
                else
                    yield Int32.Parse item
            with
            | _ ->
                printfn $"Cannot parse {item}"
                exit 0
        ]) >> List.ofSeq

let private ESC   = string (char 0x1B)
let private RESET = ESC + "[0m"
let private GRID  = ESC + "[90m"
let private SPACE_RESET = " " + ESC + "[0m"
let private SOLVED      = ESC + "[38;2;208;208;224;48;2;48;128;48m "
let private SOLVED_BG   = ESC + "[48;2;48;128;48m "
let private RULE_BG     = ESC + "[48;2;0;108;128m "
let private SOLVED_FG   = ESC + "[38;2;208;208;224m"
let private CHANGED_FG  = ESC + "[38;2;255;240;0m"
let private UNSOLVED    = ESC + "[38;2;64;64;64;48;2;176;176;176m "
let private CHANGED     = ESC + "[38;2;64;64;64;48;2;184;176;80m "
let private NORMAL_FG   = ESC + "[38;2;64;64;64m"
let private GONE_FG     = ESC + "[38;2;149;141;45m"
let private RULE_FG     = ESC + "[38;2;0;108;128m"
let private INVERT      = ESC + "[7m"
let private UNDERLINE   = ESC + "[4m"
let private ARROW_RIGHT = GRID + "▶"
let private ARROW_LEFT  = GRID + "◀"
let private RULE_UNSOLVED    = ESC + "[38;2;0;108;128;48;2;176;176;176m▋" + ESC + "[38;2;64;64;64m"
let private RULE_CHANGED     = ESC + "[38;2;0;108;128;48;2;184;176;80m▋" + ESC + "[38;2;64;64;64m"
let private RULE_OUT         = ESC + "[38;2;0;108;128;48m▐" + ESC + "[0m"
let private NO_UNDERLINE     = ESC + "[24m"
let private HIDE_CURSOR      = ESC + "[?25l"
let private SHOW_CURSOR      = ESC + "[?25h"
let private ERASE_LINE       = ESC + "[2K"
let private CURSOR_LEFT      = ESC + "[140D"
let private SAVE_POSITION    = ESC + "[s"
let private RESTORE_POSITION = ESC + "[u"

let eraseLine () = printf $"{ERASE_LINE}{CURSOR_LEFT}"
let hilite s = $"{INVERT}{s}{RESET}"
let dim s = $"{NORMAL_FG}{s}{RESET}"
let rule s = $"{RULE_FG}{s}{RESET}"

let setupTerminal () =
    try
        Console.Clear()
    with _ -> ()

    printf $"{HIDE_CURSOR}"
    printf $"{SAVE_POSITION}"

    let animation = [|"""-"""; """\"""; """|"""; """/""" |]
    let rec checkDimensions i =
        eraseLine()
        let w, h = Console.WindowWidth, Console.WindowHeight
        if w < 151 || h < 39 then
            printf $"{RESTORE_POSITION}"
            printf $" {animation[i]} Console window size is {w} x {h} characters. \
                Please enlarge it now to at least 151 x 39"
            Threading.Thread.Sleep(500)
            checkDimensions ((i + 1) % 4)
    checkDimensions 0

let restoreTerminal () =
    printf $"{SHOW_CURSOR}"
    printf $"{RESET}"

let [<Literal>] private Before = -3
let [<Literal>] private After = -2
let [<Literal>] private Start = -1
let [<Literal>] private End = 0
let [<Literal>] private blank1 = " "

let private getClearPrinter game =
    let cells = game |> Set.unionMany |> Set.toArray
    assert (cells.Length = 81)
    fun tag d ->
        let ids = cells[tag].Ids
        match d with
        | Before | After -> blank1
        | Start -> if ids.Count = 1 then SOLVED else UNSOLVED
        | End   -> SPACE_RESET
        | _     ->
            if ids.Count = 1 then
                if d <> 5 then
                    " "
                else
                    string ids.MinimumElement
            elif ids.Contains d then
                string d
            else
                " "

let private getChangePrinter newGame oldGame rule markers =
    let newCells = newGame |> Set.unionMany |> Set.toArray
    let oldCells = oldGame |> Set.unionMany |> Set.toArray
    assert (oldCells.Length = 81 && newCells.Length = 81)
    let ruleTags, ruleIds = rule.Tags, rule.Ids
    fun tag d ->
        let oldIds, newIds = oldCells[tag].Ids, newCells[tag].Ids
        match d with
        | Before -> if Set.contains tag markers then ARROW_RIGHT else blank1
        | After  -> if Set.contains tag markers then ARROW_LEFT else blank1
        | Start  ->
            if newIds.Count = 1 then
                if ruleTags.Contains tag then
                    RULE_BG
                else
                    SOLVED_BG
            elif newIds.Count = oldIds.Count then
                if ruleTags.Contains tag then
                    RULE_UNSOLVED
                else
                    UNSOLVED
            elif ruleTags.Contains tag then
                RULE_CHANGED
            else
                CHANGED
        | End ->
            if newIds.Count = 1 || not (ruleTags.Contains tag) then
                SPACE_RESET
            else
                RULE_OUT
        | _      ->
            if newIds.Count = 1 then
                if d <> 5 then
                    " "
                elif oldIds.Count = 1 then
                    $"{SOLVED_FG}{newIds.MinimumElement}"
                else
                    $"{CHANGED_FG}{UNDERLINE}{newIds.MinimumElement}{NO_UNDERLINE}"
            elif newIds.Contains d then
                if ruleTags.Contains tag && ruleIds.Contains d then
                    $"{RULE_FG}{d}{NORMAL_FG}"
                else
                    string d
            elif oldIds.Contains d then
                $"{GONE_FG}{d}{NORMAL_FG}"
            else
                " "

let inline private grid s = $"{GRID}{s}"
let private justGrid = grid("│")
let private spaceGrid = grid(" │")
let private blank = String.replicate 77 " "

let rec private buildCell f g lst =
    let c = List.head lst
    let rest = List.tail lst
    let tag: Tag = g * 9 + c
    let pre0 = if c = 0 then spaceGrid else String.Empty
    let post =
        match c with
        | 2 | 5 | 8 -> justGrid
        | _         -> blank1

    let this = [
        $"{pre0} {f tag Start}{f tag 1}{f tag 2}{f tag 3}{f tag End} {post}"
        $"{pre0}{f tag Before}{f tag Start}{f tag 4}{f tag 5}{f tag 6}{f tag End}{f tag After}{post}"
        $"{pre0} {f tag Start}{f tag 7}{f tag 8}{f tag 9}{f tag End} {post}" ]
    if rest.IsEmpty then
        this
    else
        List.map2 (+) this (buildCell f g rest)

[<TailCall>]
let rec private buildGroup f lst =
    let g = List.head lst
    let rest = List.tail lst

    match g with
    | 0     -> grid(" ┌───────────────────────┬───────────────────────┬───────────────────────┐")
    | 3 | 6 -> grid(" ├───────────────────────┼───────────────────────┼───────────────────────┤")
    | _     -> grid(" │                       │                       │                       │")
    ::
    buildCell f g [ 0 .. 8]
    @
    if rest.IsEmpty then
        grid(" └───────────────────────┴───────────────────────┴───────────────────────┘") |> List.singleton
    else
        buildGroup f rest

let inline private buildSudoku f = buildGroup f [ 0 .. 8]

let showGame showChanges stages =
    let lines =
        if List.length stages >= 2 then
            let newGame, oldGame, rule, markers = stages[0].Game, stages[1].Game, stages[0].Rule, stages[0].Markers
            let newSudoku =
                if showChanges then getChangePrinter newGame oldGame rule markers else getClearPrinter newGame
                |> buildSudoku
                |> Seq.map (fun s -> s + "   ")
            let oldSudoku = oldGame |> getClearPrinter |> buildSudoku
            Seq.map2 (+) newSudoku oldSudoku
        else
            stages.Head.Game |> getClearPrinter |> buildSudoku |> Seq.map (fun s -> s + blank)
    printf $"{ERASE_LINE}{RESTORE_POSITION}"
    for line in lines do
        printfn $"{line}"
    printf $"{RESET}"

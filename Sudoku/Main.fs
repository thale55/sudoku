﻿// Sudoku r2023
// Run in Windows Terminal or some other modern terminal with native ANSI color support.
// Not Windows Command Prompt.

module Sudoku.Main

[<TailCall>]
let rec private iterativeSolver settings stages =
    Io.showGame settings.ShowChanges stages
    let { Game = game; SolverNames = solverNames; Rule = rule } = List.head stages
    let control, newSettings = Controller.control (Game.getWeight game) solverNames rule settings
    match control with
    | Show -> iterativeSolver newSettings stages
    | Solve solvers ->
        let newSolver, newGame = Runner.solve game solvers
        if newSolver.IsEmpty then
            iterativeSolver newSettings stages
        else
            let newStage = Stage.from2 newGame newSolver.Name
            iterativeSolver newSettings (newStage::stages)
    | SolveAll solvers ->
        let newStages = Runner.solveAll solvers stages
        iterativeSolver newSettings newStages
    | SolveOnce selectedSolver ->
        let newRule, newGame, markers = Runner.solveOnce game selectedSolver
        if newRule.IsEmpty then
            iterativeSolver newSettings stages
        else
            let newStage = Stage.from4 newGame selectedSolver.Name newRule markers
            iterativeSolver newSettings (newStage::stages)
    | Drop ->
        iterativeSolver newSettings (if stages.Length > 1 then stages.Tail else stages)
    | Quit ->
        Io.restoreTerminal()
        exit 0

match Io.checkArgument () with
| Io.IsFile name -> 
    let loadedRules = Io.loadRules name
    let game = Game.from loadedRules
    Io.setupTerminal()
    iterativeSolver Controller.initialSettings [Stage.from game]
| Io.IsDirectory (name, runs) ->
    Stats.run name runs
| Io.IsNothing ->
    printfn "Usage: Sudoku <data file> or Sudoku <directory of data files>"

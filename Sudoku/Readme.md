# Documentation

## These are my rules about rules within a group

**tag rule** ```Tags.Count <= Ids.Count```  
*'These tags can contain these ids only. No other ids are possible in these tags'*

**single-tag rule** ```Tags.Count == 1```  
*'This tag can contain these ids only. No other ids are possible in this tag'*

**id rule** ```Ids.Count <= Tags.Count```  
*'These ids are possible in these tags only. No other tags can contain these ids'*

**single-id rule** ```Ids.Count == 1```  
*'This id is possible in these tags only. No other tags can contain this id'*

**perfect rule** ```Tags.Count == Ids.Count```  
*'Both a tag rule and an id rule'*

**solved rule** ```Tags.Count == Ids.Count == 1```  
*'Smallest perfect rule'*

**unsolved rule** ```Tags.Count > 1 or  Ids.Count > 1```  
*'Any rule that is not solved'*

## Semantics

```Cell``` is always a single-tag rule.  
```Group``` and ```Game``` contain cells.
```Rules``` can contain all kinds of rules.  

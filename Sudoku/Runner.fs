﻿module Sudoku.Runner

open Types

let private apply (rule: Rule) (group: Group) : Group =
    let cells = group |> Set.filter (fun cell -> rule.ContainsTag cell.OnlyTag)
    let g2 =
        if rule.IsTagRule then
            cells |> Set.fold (fun gr cell -> gr - cell + (cell &&& rule)) group
        else
            group
    if rule.IsIdRule then
        group - cells |> Set.fold (fun gr cell -> gr - cell + (cell / rule)) g2
    else
        g2

let inline private tryApply game rule =
    let groups, rest = game |> List.partition (Group.isApplicableTo rule)
    let newGroups = groups |> List.map (apply rule)
    if Game.getRawWeight newGroups <> Game.getRawWeight groups then
        struct (true, Game.unify (rest @ newGroups))
    else
        struct (false, game)

let solveOnce game solver : Rule * Game * Tag Set =
    seq {
        for struct (rule, markers) in solver.GetRules game do
            let struct (ruleWasApplied, g) = tryApply game rule
            if ruleWasApplied then
                yield rule, g, markers |> Rules.allTags
                ()  // yield break
        yield Rule.Zero, game, Set.empty   // Seq.head in case no rule made a difference
    } |> Seq.head

[<TailCall>]
let rec solve game =
    function
    | [] -> Solver.Zero, game
    | solver::rest ->
        let initialState = struct (game, false)
        let ruleProvider = solver.GetRules game |> Seq.map fstVal
        let struct (newGame, gameChanged) =
            Seq.fold (fun struct (game, changed) rule ->
                let struct (ruleWasApplied, g) = tryApply game rule
                struct (g, ruleWasApplied || changed)
            ) initialState ruleProvider
        if gameChanged then
            assert (newGame |> List.forall (Set.forall Rule.notEmpty))
            solver, newGame
        else
            solve game rest

[<TailCall>]
let rec solveAll solvers stages =
    let { Game = game } = List.head stages
    let solver, newGame = solve game solvers
    if solver.IsEmpty then
        Stage.unionOf stages :: stages.Tail
    else
        let newStages = Stage.from2 newGame solver.Name :: stages
        if Game.isSolved newGame then
            Stage.unionOf newStages :: newStages.Tail
        else
            solveAll solvers newStages

﻿module Sudoku.Stats

open System.Diagnostics
open System.IO

type Result = { Average: float; Solved: int; Millis: int }

let private defaultRuns = [
    [ 1 ]
    [ 1; 2 ]
    [ 1; 3 ]
    [ 1; 4 ]
    [ 1; 5 ]
    [ 1; 6 ]
    [ 1; 2; 3; 4; 5 ]
    [ 1; 2; 3; 4; 5; 6 ]
]

let private printHeader (solvers: Solver array) =
    solvers |> Seq.mapi (fun i { Name = name } -> $" {i + 1}: {name} ") |> String.concat "" |> printfn "%s"
    printfn " ┌─────────────┬──────────────────────┬────────────────────┬──────────────┐"
    printfn " │   Solvers   │ Avg remaining weight │ Solved games/total │ Milliseconds │"
    printfn " ├─────────────┼──────────────────────┼────────────────────┼──────────────┤"

let private printFooter () =
    printfn " └─────────────┴──────────────────────┴────────────────────┴──────────────┘"

let inline private padLeft n (s: string) = s.PadLeft n

let private checkRuns runs =
    let flat = List.concat runs
    if List.min flat < 1 || List.max flat > Controller.allSolvers.Length then
        printfn "Illegal arguments"
        exit 0
    runs

let private printResult total toRun { Average = avg; Solved = solved; Millis = millis } =
    let items = toRun |> Seq.map string |> String.concat "," |> padLeft 11
    let weight = $"%.3f{avg}" |> padLeft 20
    let ratio = $"{solved}/{total}" |> padLeft 18
    let ms = $"{millis}" |> padLeft 12
    printfn $" │ {items} │ {weight} │ {ratio} │ {ms} │"

[<TailCall>]
let rec private runGame solvers game =
    let solver, newGame = Runner.solve game solvers
    if solver.IsEmpty then
        Game.getWeight newGame
    elif Game.isSolved newGame then
        0
    else
        runGame solvers newGame

let private runGames (stopWatch: Stopwatch) games solvers =
    let startMillis = stopWatch.ElapsedMilliseconds
    let weights = games |> Array.map (runGame solvers)
    let millis = stopWatch.ElapsedMilliseconds - startMillis |> int
    let avg = float (Array.sum weights) / float games.Length
    let solved = weights |> Seq.where ((=) 0) |> Seq.length
    { Average = avg; Solved = solved; Millis = millis }

let run directoryPath runs =
    let allSolvers = Controller.allSolvers |> List.toArray
    let games =
        Directory.EnumerateFiles(directoryPath, "*.d", SearchOption.AllDirectories)
        |> Seq.map (Io.loadRules >> Game.from)
        |> Seq.toArray
    if games.Length = 0 then
        printfn $"No sudoku data files were found in {directoryPath}"
        exit 0
    let allRuns = if Array.isEmpty runs then defaultRuns else Io.parseRuns allSolvers.Length runs |> checkRuns
    let runner = runGames (Stopwatch.StartNew()) games
    let printer = printResult games.Length
    printHeader allSolvers
    // Warm-up for Stopwatch
    runGames (Stopwatch.StartNew()) games[3..4] Controller.allSolvers |> ignore
    for toRun in allRuns do
        let solvers = toRun |> List.map ((+) -1 >> Array.get allSolvers)
        runner solvers |> printer toRun
    printFooter()

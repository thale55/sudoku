namespace Sudoku

open System.Numerics

module Types =
    let inline fstVal struct (a, _) = a
    let inline sndVal struct (_, b) = b
    let inline (>><) func1 func2 x y = func2 (func1 x y)

module Seq =
    let inline toString (objs:'a seq) = objs |> Seq.map string |> String.concat ","
    let allSubsets maxSubsetSize source =
        let inline worker vec num =
            let n = Array.length vec - 1
            seq { for i = 0 to n do let k = 1u <<< i in if k &&& num <> 0u then yield vec[i] }
        let vec = Seq.toArray source
        let count = (1u <<< Array.length vec) - 1u
        seq {
            for num = 1u to count do
                if BitOperations.PopCount(num) <= maxSubsetSize then
                    yield worker vec num }
    let pairings source =
        let vec = Seq.toArray source
        let n1 = vec.Length - 1
        let n2 = n1 - 1
        seq {
            for i = 0 to n2 do
                for j = i + 1 to n1 do
                    yield struct (vec[i], vec[j]) }

module List =
    let inline notEmpty lst = not (List.isEmpty lst)

module Set =
    let inline notEmpty source = not (Set.isEmpty source)

type Tag = int
type Id = int

type Rule = { Tags:Tag Set; Ids:Id Set } with
    member inline this.IsTagRule = this.Tags.Count <= this.Ids.Count
    member inline this.IsSingleTagRule = this.Tags.Count = 1
    member inline this.IsIdRule = this.Ids.Count <= this.Tags.Count
    member inline this.IsSingleIdRule = this.Ids.Count = 1
    member inline this.IsPerfect = this.Tags.Count = this.Ids.Count
    member inline this.IsSolved = this.Tags.Count = 1 && this.Ids.Count = 1
    member inline this.IsUnsolved = this.Tags.Count > 1 || this.Ids.Count > 1
    member inline this.IsEmpty = this.Ids.IsEmpty || this.Tags.IsEmpty
    member inline this.OnlyTag = Seq.exactlyOne this.Tags
    member inline this.OnlyId = Seq.exactlyOne this.Ids
    member inline this.ContainsTag tag = this.Tags.Contains tag
    member inline this.ContainsId id = this.Ids.Contains id
    override this.ToString() = $"{Seq.toString this.Tags}: {Seq.toString this.Ids}"

    static member val allTagsSet = Set<Tag> [ 0 .. 80 ]
    static member val allIdsSet = Set<Id> [ 1 .. 9 ]
    static member val allIdsArray = [| 1 .. 9 |]
    static member val Zero = { Tags = Set.empty; Ids = Set.empty }
    static member inline (+) (a, b) = { Tags = a.Tags + b.Tags; Ids = a.Ids + b.Ids }
    static member inline (&&&) (a, b) = { Tags = Set.intersect a.Tags b.Tags; Ids = Set.intersect a.Ids b.Ids }
    static member inline (/) (a, b) = { Tags = a.Tags; Ids = a.Ids - b.Ids }
    static member inline (+) (a: Rule Set, b: Rule) = Set.add b a
    static member inline (-) (a: Rule Set, b: Rule) = Set.remove b a
    static member inline containsId id { Ids = s } = s.Contains id
    static member inline notEmpty (rule: Rule) = not rule.IsEmpty
    static member inline tagCount { Tags = tags } = tags.Count
    static member inline from tags ids =
        assert Set.isSubset tags Rule.allTagsSet
        assert Set.isSubset ids Rule.allIdsSet
        assert not tags.IsEmpty
        assert not ids.IsEmpty
        { Tags = tags; Ids = ids }
    static member fromSolved tag id = Rule.from (Set.singleton tag) (Set.singleton id)
    static member fromTag tag = Rule.from (Set.singleton tag) Rule.allIdsSet
    static member inline fromSingleId tags id = { Tags = tags; Ids = Set.singleton id }

type Rules = Rule seq
type Cell  = Rule
type Group = Cell Set
type Game  = Group list

type Solver = { Name: string; GetRules: Game -> struct (Rule * Set<Rule>) seq } with
    member this.IsEmpty = this.Name.Length = 0
    static member val Zero = { Name = System.String.Empty; GetRules = fun _ -> Seq.empty }

type Stage = { Game: Game; SolverNames: string Set; Rule: Rule; Markers: Tag Set} with
    static member inline from game =
        { Game = game; SolverNames = Set.empty; Rule = Rule.Zero; Markers = Set.empty }
    static member inline from2 game name =
        { Game = game; SolverNames = Set.singleton name; Rule = Rule.Zero; Markers = Set.empty }
    static member inline from4 game name rule markers =
        { Game = game; SolverNames = Set.singleton name; Rule = rule; Markers = markers }
    static member unionOf stages =
        let { Game = game; Rule = rule; Markers = markers } = List.head stages
        let names = stages |> Seq.map _.SolverNames |> Set.unionMany
        { Game = game; SolverNames = names; Rule = rule; Markers = markers }

module Group =
    let empty = Set.empty<Cell>
    let allTags: Group -> Tag Set = Set.map _.OnlyTag
    let inline unsolvedCells (group:Group) = group |> Seq.where _.IsUnsolved
    let inline isApplicableTo rule group = rule.Tags.IsSubsetOf (allTags group)
    let inline getTagsById id = Set.filter (Cell.containsId id) >> Set.map _.OnlyTag
    let asSingleIdRules group =
        Rule.allIdsArray
        |> Seq.map (fun id -> Rule.fromSingleId (getTagsById id group) id)
    let inline getWeight (group:Group) = group |> Seq.sumBy _.Ids.Count

module Game =
    let empty = List.empty<Group>
    let private initial () : Game =
        let newGroupH row : Group = seq {
            let offset = row * 9 in
                for tag = offset to offset + 8 do
                    yield Rule.fromTag tag } |> set
        let newGroupV col : Group = seq {
            for row = 0 to 8 do
                yield Rule.fromTag (col + row * 9) } |> set
        let newGroupB i j : Group = seq {
            for r = 0 to 2 do
                for c = 0 to 2 do
                    yield Rule.fromTag (j * 3 + c + i * 27 + r * 9) } |> set
        [ for row = 0 to 8 do yield newGroupH row ] @
        [ for col = 0 to 8 do yield newGroupV col ] @
        [ for i = 0 to 2 do for j = 0 to 2 do yield newGroupB i j ]
    // Tag number [0..80] to set of initial containing group numbers [0..26]
    let tagToGroups =
        let indexedGroups = initial() |> List.map Group.allTags |> List.indexed
        Rule.allTagsSet
        |> Seq.map (fun tag ->
            indexedGroups
            |> Seq.where (snd >> Set.contains tag)
            |> Seq.map fst
            |> set)
        |> Array.ofSeq
    let inCommonGroup tag1 tag2 =
        Set.intersect tagToGroups[tag1] tagToGroups[tag2] |> Set.notEmpty
    let from (loadedRules: Rules) : Game =
        let loaded = loadedRules |> Seq.map (fun rule -> rule.OnlyTag, rule) |> Map.ofSeq
        initial ()
        |> List.map (
            Set.map (fun rule ->
                match Map.tryFind rule.OnlyTag loaded with
                | Some r2 -> r2
                | None    -> rule))
    let getRawWeight = List.sumBy Group.getWeight
    let inline getWeight game =
        let w = getRawWeight game
        assert (w % 3 = 0)
        w / 3 - 81
    let isSolved = List.forall (Group.getWeight >> (=) 9)
    // After applying rules, game |> Seq.concat |> Seq.distinct |> Seq.length can be > 81
    let unify (game: Game) : Game =
        let cellMap =
            game
            |> Seq.concat
            |> Seq.groupBy _.OnlyTag
            |> Seq.map (fun (tag, cells) -> tag, cells |> Seq.reduce (&&&))
            |> Map.ofSeq
        game
        |> List.map (Set.map (fun cell -> cellMap[cell.OnlyTag]))

module Rules =
    let empty = Seq.empty<Rule>
    let allTags: Rules -> Tag Set = Seq.collect _.Tags >> set
